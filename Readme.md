# Pipeworld (or 3D snake)

made by Margo Argenti, Antonio Azara, Giorgio Gori as part of the Computer Graphics course by Kai Hormann at Universita' della Svizzera Italiana

### Inspiration
The game is inspired by the traditional Snake series and by [Counterclockwise](http://www.16x16.org/games/ccw/)

### Goal
The goal of the game is to collect gems and avoid hitting the pipes. Pipes are created as the player moves.

### Controls
- arrow keys or w,a,s,d to turn.
- Space or ESC to pause
- 1 to toggle the toon shader on/off
- R to reset after death
- Q to quit (only when in pause)

### World
The playable world is a 10 x 10 x 10 cube, it is recursively closed: you will never see a wall, but once going in a direction for a little bit, you will find your own trail in front of you.

### Technologies
C++, OpenGL, Qt

### How to build
use `qmake`