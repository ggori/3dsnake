#ifndef GLRender_CubePipe_h
#define GLRender_CubePipe_h

#include "Object.h"

class Cube : public Pipe
{
public:
    Cube()
    : Pipe() {}
    
    void draw() {
        draw3();
    }
    
    // Uses triangles. Normals good for cube lighting with smooth shading.
    void draw3() {
        glColor3f(_color.r(), _color.g(), _color.b());
        glShadeModel( GL_FLAT );
        glBegin( GL_TRIANGLES );
        
        glNormal3f(0, 0, 1);
        glVertex3f(-.5, -.5, .5);
        glVertex3f(.5, -.5, .5);
        glVertex3f(-.5, .5, .5);
        glVertex3f(-.5, .5, .5);
        glVertex3f(.5, -.5, .5);
        glVertex3f(.5, .5, .5);
        
        glNormal3f(1, 0, 0);
        glVertex3f(.5, -.5, .5);
        glVertex3f(.5, -.5, -.5);
        glVertex3f(.5, .5, .5);
        glVertex3f(.5, .5, .5);
        glVertex3f(.5, -.5, -.5);
        glVertex3f(.5, .5, -.5);
        
        glNormal3f(0, 0, -1);
        glVertex3f(.5, -.5, -.5);
        glVertex3f(-.5, -.5, -.5);
        glVertex3f(.5, .5, -.5);
        glVertex3f(.5, .5, -.5);
        glVertex3f(-.5, -.5, -.5);
        glVertex3f(-.5, .5, -.5);
        
        glNormal3f(-1, 0, 0);
        glVertex3f(-.5, -.5, -.5);
        glVertex3f(-.5, -.5, .5);
        glVertex3f(-.5, .5, -.5);
        glVertex3f(-.5, .5, -.5);
        glVertex3f(-.5, -.5, .5);
        glVertex3f(-.5, .5, .5);
        
        glNormal3f(0, -1, 0);
        glVertex3f(.5, -.5, -.5);
        glVertex3f(-.5, -.5, -.5);
        glVertex3f(.5, -.5, .5);
        glVertex3f(.5, -.5, .5);
        glVertex3f(-.5, -.5, -.5);
        glVertex3f(-.5, -.5, .5);
        
        glNormal3f(0, 1, 0);
        glVertex3f(-.5, .5, .5);
        glVertex3f(.5, .5, .5);
        glVertex3f(-.5, .5, -.5);
        glVertex3f(-.5, .5, -.5);
        glVertex3f(.5, .5, .5);
        glVertex3f(.5, .5, -.5);
        
        glEnd();
    }
    
    // Uses a triangle strip. Normal approximate a sphere.
    void draw2() {
        glColor3f(_color.r(), _color.g(), _color.b());
        glShadeModel( GL_FLAT );
        glBegin(GL_TRIANGLE_STRIP);
        glNormal3f(-.5, .5, .5);
        glVertex3f(-.5, .5, .5);
        glNormal3f(.5, .5, .5);
        glVertex3f(.5, .5, .5);
        glNormal3f(-.5, -.5, .5);
        glVertex3f(-.5, -.5, .5);
        glNormal3f(.5, -.5, .5);
        glVertex3f(.5, -.5, .5);
        glNormal3f(.5, -.5, -.5);
        glVertex3f(.5, -.5, -.5);
        glNormal3f(.5, .5, .5);
        glVertex3f(.5, .5, .5);
        glNormal3f(.5, .5, -.5);
        glVertex3f(.5, .5, -.5);
        glNormal3f(-.5, .5, .5);
        glVertex3f(-.5, .5, .5);
        glNormal3f(-.5, .5, -.5);
        glVertex3f(-.5, .5, -.5);
        glNormal3f(-.5, -.5, .5);
        glVertex3f(-.5, -.5, .5);
        glNormal3f(-.5, -.5, -.5);
        glVertex3f(-.5, -.5, -.5);
        glNormal3f(.5, -.5, -.5);
        glVertex3f(.5, -.5, -.5);
        glNormal3f(-.5, .5, -.5);
        glVertex3f(-.5, .5, -.5);
        glNormal3f(.5, .5, -.5);
        glVertex3f(.5, .5, -.5);
        glEnd();
    }
    
    // Uses a triangle strip. Normals good for flat shading.
    void draw1() {
        glColor3f(_color.r(), _color.g(), _color.b());
        glShadeModel( GL_FLAT );
        glBegin(GL_TRIANGLE_STRIP);
        glVertex3f(-.5, .5, .5);
        glVertex3f(.5, .5, .5);
        glNormal3f(0, 0, 1);
        glVertex3f(-.5, -.5, .5);
        glVertex3f(.5, -.5, .5);
        glNormal3f(0, -1, 0);
        glVertex3f(.5, -.5, -.5);
        glNormal3f(1, 0, 0);
        glVertex3f(.5, .5, .5);
        glVertex3f(.5, .5, -.5);
        glNormal3f(0, 1, 0);
        glVertex3f(-.5, .5, .5);
        glVertex3f(-.5, .5, -.5);
        glNormal3f(-1, 0, 0);
        glVertex3f(-.5, -.5, .5);
        glVertex3f(-.5, -.5, -.5);
        glNormal3f(0, -1, 0);
        glVertex3f(.5, -.5, -.5);
        glNormal3f(0, 0, -1);
        glVertex3f(-.5, .5, -.5);
        glVertex3f(.5, .5, -.5);
        glEnd();
    }
};

#endif
