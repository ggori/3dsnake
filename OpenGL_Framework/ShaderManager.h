// Implementation courtesy of Alessio Aurecchia, Jacques Dafflon & Giovanni Vivani

#ifndef __GLRender__ShaderManager__
#define __GLRender__ShaderManager__

#ifndef __APPLE__
#define GL_GLEXT_PROTOTYPES
#include <GL/glu.h>
#include <GL/glcorearb.h>
#endif

#include <iostream>
#include <fstream>
#include "Base.h"

class ShaderManager {
    
public:
    static GLhandleARB loadShader(const char * vertex_path, const char * fragment_path);

private:
    static std::string readFile(const char * filePath);
};

#endif
