#ifndef BASE_H
#define BASE_H

/***************************************************************************
 Base.h
 Comment:  This file contains all basic definitions.
 ***************************************************************************/

#define PI 3.141592653589793

// Axis
enum Axis_t {
    AXIS_X, AXIS_Y, AXIS_Z
};

// STL includes
#include <iostream>
#include <assert.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include <float.h>
#include <queue>
#include <set>
#include <iostream>
#include <QtOpenGL>
#include <QGLWidget>
#include <QTimer>

// Utility
#include "Point3i.h"
#include "Point3d.h"
#include "Color.h"
#include "ColorGenerator.h"

// Objects
#include "Object.h"
#include "Empty.h"
#include "Pipe.h"
#include "FittingPipe.h"
#include "StraightPipe.h"
#include "HeadPipe.h"
#include "Pickup.h"
#include "PickupCube.h"
#include "PickupSphere.h"

//-----------------------------------------------------------------------------
/** color modes */
enum ColorMode {
    CMNone,
    PerVert,
    PerFace
};


#endif
