/************************************************************************/
/* guards                                                               */
/************************************************************************/
#ifndef CCANVAS_H
#define CCANVAS_H

// Main game parameters
#define CUBE_SIZE 10  // side of the cube
#define MAX_LIVES 3  // starting and maximum number of lives
#define PICKUPS_QUANTITY 5  // number of pickups present in the game at any time
#define SPAWN_RANGE 1  // radius of the area that the pickups will destroy when spawning
#define FOG_DENSITY 0.25 // fog level (from 0 [none] to 1 [max])

#define GL_GLEXT_PROTOTYPES

#include "Base.h"

using namespace std;

/************************************************************************/
/* Canvas to draw                                                       */
/************************************************************************/
enum state_t {MOVING, TURNING};

class CCanvas : public QGLWidget {
    Q_OBJECT
    
public:
    
    explicit CCanvas(QWidget *parent = 0) : QGLWidget(parent),tau(0.0) {
        QTimer *timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(updateGL()));
        timer->start(10);
    }
    void keyPressEvent(QKeyEvent *);

protected:
    
    void initializeGL();
    void resetWorld();
    void resizeGL(int width, int height);
    void paintGL();
    void update();
    float getDensity();
    void draw();
    void drawText(int x, int y, QString text, QFont font);
    void spawnPickup();
    
private:
    
    // List of vertices and triangles
    Object * objects[CUBE_SIZE][CUBE_SIZE][CUBE_SIZE];
    
    Point3i position;
    Point3i direction;
    Point3i old_direction;
    Point3i up;
    
    state_t state;
    
    bool fullscreened;
    bool paused;
    bool shaderActive;
    bool dead;
    
    float soFarSoGood;
    
    GLhandleARB program;
    
    double tau;
    
    int lives;
    int score;

    float density;
};
#endif 
