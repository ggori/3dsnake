#ifndef FITTING_PIPE_H
#define FITTING_PIPE_H

#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif

class FittingPipe : public Pipe {
private:
    static GLuint listCylinder;
    static GLuint listSphere;
    static bool initialized;
    static GLUquadric * quadric;
    
    Point3i _from;
    Point3i _to;
    
    void rotateDraw(Point3i v);
    
public:
    FittingPipe(Point3i from, Point3i to);

    void draw();
};
#endif // FITTING_PIPE_H
