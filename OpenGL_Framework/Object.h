#ifndef OBJECT_H
#define OBJECT_H

#include "Color.h"

class Object {
protected:
    Color3d _color;
    
public:
    Object(Color3d c)
    : _color(c) {}
    
    virtual ~Object() {}

    virtual bool exists() = 0;
    
    inline Color3d color() const { return _color; }
    
    virtual void draw() = 0;
    
    virtual bool isObstacle() = 0;
    
    virtual bool isPickup() = 0;
};

#endif //OBJECT_H

