#ifndef __GLRender__HeadPipe__
#define __GLRender__HeadPipe__

#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif

class HeadPipe {
private:
    static GLuint listCylinder;
    static GLuint listSphere;
    static bool initialized;
    static GLUquadric * quadric;
    
    static void init();
    
public:
    static void draw(Point3i v);
};

#endif
