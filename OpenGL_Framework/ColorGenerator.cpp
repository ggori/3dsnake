#include "Base.h"
#include "Color.h"

#include "ColorGenerator.h"

float ColorGenerator::tau = 0;

Color3d ColorGenerator::generate_color() {
    tau += 0.1;
    return get_current_color();
}

Color3d ColorGenerator::get_current_color() {
    float center = 0.7; // magic numbers!
    float width = 0.3;
    return Color3d(sin(tau + 0*PI/3) * width + center,
                   sin(tau + 2*PI/3) * width + center,
                   sin(tau + 4*PI/3) * width + center);
}
