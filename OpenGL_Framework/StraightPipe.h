#ifndef STRAIGHTPIPE_H
#define STRAIGHTPIPE_H

class StraightPipe : public Pipe {
private:
    Axis_t _axis;
    
public:
    static GLuint list;
    static bool initialized;
    
    StraightPipe(Axis_t a);
    
    void draw();

};

#endif //STRAIGHTPIPE
