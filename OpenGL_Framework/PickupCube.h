#ifndef GLRender_PickupCube_h
#define GLRender_PickupCube_h

#define SQRT3over3 .577350269

#include "Pickup.h"


class PickupCube: public Pickup {
private:
    static GLuint list;
    static bool initialized;
    
    float tau;
    
public:
    PickupCube();
    
    void draw();
};

#endif
