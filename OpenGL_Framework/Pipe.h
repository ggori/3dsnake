#ifndef PIPE_H
#define PIPE_H

#include "ColorGenerator.h"
#include "Object.h"

class Pipe : public Object {
public:
    Pipe()
        : Object(ColorGenerator::generate_color()) {}
    
    virtual ~Pipe() {}
    
    bool exists() { return true; }
    
    bool isObstacle() { return true; }
    
    bool isPickup() { return false; }
};

#endif //PIPE_H
