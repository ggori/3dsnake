#include "Base.h"
#include "Pickup.h"
#include "PickupCube.h"

// Static variables
GLuint PickupCube::list = 0;
bool PickupCube::initialized = false;

PickupCube::PickupCube() {
    tau = 1;
    
    if (!initialized) {
        initialized = true;
        
        list = glGenLists(1);
        glNewList(list, GL_COMPILE);
        
        glShadeModel( GL_SMOOTH );
        
        glBegin(GL_TRIANGLES);
        glNormal3f(SQRT3over3, SQRT3over3, SQRT3over3);
        glVertex3f(0, .5, 0);
        glVertex3f(0, 0, .5);
        glVertex3f(.5, 0, 0);
        
        glNormal3f(SQRT3over3, SQRT3over3, -SQRT3over3);
        glVertex3f(0, .5, 0);
        glVertex3f(.5, 0, 0);
        glVertex3f(0, 0, -.5);
        
        glNormal3f(-SQRT3over3, SQRT3over3, -SQRT3over3);
        glVertex3f(0, .5, 0);
        glVertex3f(0, 0, -.5);
        glVertex3f(-.5, 0, 0);
        
        glNormal3f(-SQRT3over3, SQRT3over3, SQRT3over3);
        glVertex3f(0, .5, 0);
        glVertex3f(-.5, 0, 0);
        glVertex3f(0, 0, .5);
        
        glNormal3f(SQRT3over3, -SQRT3over3, SQRT3over3);
        glVertex3f(0, -.5, 0);
        glVertex3f(.5, 0, 0);
        glVertex3f(0, 0, .5);
        
        glNormal3f(SQRT3over3, -SQRT3over3, -SQRT3over3);
        glVertex3f(0, -.5, 0);
        glVertex3f(0, 0, -.5);
        glVertex3f(.5, 0, 0);
        
        glNormal3f(-SQRT3over3, -SQRT3over3, -SQRT3over3);
        glVertex3f(0, -.5, 0);
        glVertex3f(-.5, 0, 0);
        glVertex3f(0, 0, -.5);
        
        glNormal3f(-SQRT3over3, -SQRT3over3, SQRT3over3);
        glVertex3f(0, -.5, 0);
        glVertex3f(0, 0, .5);
        glVertex3f(-.5, 0, 0);
        
        glEnd();
        
        glEndList();
    }
}

// draw with triangles, good normals for toon or gauraud shading.
void PickupCube::draw() {
    glColor3f(_color.r(), _color.g(), _color.b());
    tau += .2;
    glPushMatrix();
    glRotatef(tau, 0, 1, 0);
    glRotatef(tau*2, 1, 0, 0);
    glRotatef(tau*3, 0, 0, 1);

    glCallList(list);
    
    glPopMatrix();
}


/*
// draw with 2 triangle fans. Good enough for flat shading.
void draw2() {
    glColor3f(_color.r(), _color.g(), _color.b());
    tau += .2;
    glPushMatrix();
    glRotatef(tau, 0, 1, 0);
    glShadeModel( GL_FLAT );
    
    glBegin(GL_TRIANGLE_FAN);
    
    glVertex3f(0, .5, 0);
    glVertex3f(0, 0, .5);
    
    glNormal3f(SQRT3over3, SQRT3over3, SQRT3over3);
    glVertex3f(.5, 0, 0);
    
    glNormal3f(SQRT3over3, SQRT3over3, -SQRT3over3);
    glVertex3f(0, 0, -.5);
    
    glNormal3f(-SQRT3over3, SQRT3over3, -SQRT3over3);
    glVertex3f(-.5, 0, 0);
    
    glNormal3f(-SQRT3over3, SQRT3over3, SQRT3over3);
    glVertex3f(0, 0, .5);
    
    glEnd();
    
    glBegin(GL_TRIANGLE_FAN);
    
    glVertex3f(0, -.5, 0);
    glVertex3f(0, 0, .5);
    
    glNormal3f(SQRT3over3, -SQRT3over3, SQRT3over3);
    glVertex3f(.5, 0, 0);
    
    glNormal3f(SQRT3over3, -SQRT3over3, -SQRT3over3);
    glVertex3f(0, 0, -.5);
    
    glNormal3f(-SQRT3over3, -SQRT3over3, -SQRT3over3);
    glVertex3f(-.5, 0, 0);
    
    glNormal3f(-SQRT3over3, -SQRT3over3, SQRT3over3);
    glVertex3f(0, 0, .5);
    
    glEnd();
    
    glPopMatrix();
}
*/