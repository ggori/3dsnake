#include "Base.h"
#include "Pipe.h"
#include "StraightPipe.h"

// Constructor
GLuint StraightPipe::list = 0;
bool StraightPipe::initialized = false;
StraightPipe::StraightPipe(Axis_t a) {
    _axis = a;
    if (!initialized) {
        initialized = true;
        
        list = glGenLists(1);
        glNewList(list, GL_COMPILE);
        const float center_x = 0;
        const float center_y = 0;
        const float radius = 0.45;
        const float z_low = -0.5;
        const float z_high = 0.5;
        const float n = 20;
        
        const float theta = 2 * PI / float(n);  // angle for each side-segment
        const float c = cosf(theta);  //precalculate the sine and cosine
        const float s = sinf(theta);
        float t;
        float x = radius; //we start at angle = 0
        float y = 0;
        glBegin(GL_TRIANGLE_FAN);
        glNormal3f(0, 0, -1);
        glVertex3f(center_x, center_y, z_low);
        for (int i=0; i <= n; ++i) {
            glVertex3d(x + center_x, y + center_y, z_low);
            t = x;
            x = c * x - s * y;
            y = s * t + c * y;
        }
        glEnd();
        
        x = radius;
        y = 0;
        glBegin(GL_TRIANGLE_FAN);
        glNormal3f(center_x, center_y, 1);
        glVertex3f(center_x, center_y, z_high);
        for (int i=0; i <= n; ++i) {
            glVertex3d(x + center_x, y + center_y, z_high);
            t = x;
            x = c * x - s * y;
            y = s * t + c * y;
        }
        glEnd();
        
        x = radius;
        y = 0;
        glBegin(GL_TRIANGLE_STRIP);
        for (int i=0; i <= n; ++i) {
            glNormal3f(x, y, 0);
            glVertex3f(x + center_x, y + center_y, z_low);
            glVertex3f(x + center_x, y + center_y, z_high);
            t = x;
            x = c * x - s * y;
            y = s * t + c * y;
        }
        glEnd();
        glEndList();
    }
}

void StraightPipe::draw()
{
    glShadeModel( GL_SMOOTH );
    glPushMatrix();
    if (_axis == AXIS_X) {
        glRotatef(90, 0, 1, 0);
    } else if (_axis == AXIS_Y) {
        glRotatef(90, 1, 0, 0);
    } // AXIS_Z is default
    
    glColor3f(_color.r(), _color.g(), _color.b());
    
    glCallList(list);
    
    glPopMatrix();
}
