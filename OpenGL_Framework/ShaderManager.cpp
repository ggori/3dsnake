// Implementation courtesy of Alessio Aurecchia, Jacques Dafflon & Giovanni Vivani

#include "ShaderManager.h"

std::string ShaderManager::readFile(const char * filePath) {
    std::string content;
    std::ifstream fileStream(filePath, std::ios::in);
    
    if(!fileStream.is_open()) {
        std::cerr << "Could not read file " << filePath << ". File does not exist." << std::endl;
        return "";
    }
    
    std::string line = "";
    while(!fileStream.eof()) {
        std::getline(fileStream, line);
        content.append(line + "\n");
    }
    
    fileStream.close();
    return content;
}

GLhandleARB ShaderManager::loadShader(const char * vertex_path, const char * fragment_path) {
    GLhandleARB vertexShader = glCreateShaderObjectARB(GL_VERTEX_SHADER);
    GLhandleARB fragmentShader = glCreateShaderObjectARB(GL_FRAGMENT_SHADER);
    
    std::string vertShaderStr = readFile(vertex_path);
    std::string fragShaderStr = readFile(fragment_path);
    
    const char * vertShaderSrc = vertShaderStr.c_str();
    const char * fragShaderSrc = fragShaderStr.c_str();
    
    glShaderSourceARB(vertexShader, 1, &vertShaderSrc, NULL);
    glShaderSourceARB(fragmentShader, 1, &fragShaderSrc, NULL);
    
    glCompileShaderARB(vertexShader);
    glCompileShaderARB(fragmentShader);
    
    GLhandleARB program = glCreateProgramObjectARB();
    
    glAttachObjectARB(program, vertexShader);
    glAttachObjectARB(program, fragmentShader);
    
    glLinkProgramARB(program);
    
    assert(glGetError()==0);
    
    return program;
}
