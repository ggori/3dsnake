#ifndef PICKUP_H
#define PICKUP_H

#define SQRT3over3 .577350269

#include "Object.h"


class Pickup: public Object {
private:
    float tau;
    
public:
    Pickup()
    : Object(Color3d(1, 0, 0)) {
        tau = 1;
    }
    
    virtual ~Pickup() {}
    
    bool exists() { return true; }
    
    bool isObstacle() { return false; }
    
    bool isPickup() { return true; }
};

#endif //PICKUP_H
