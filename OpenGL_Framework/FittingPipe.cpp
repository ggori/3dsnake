#include "Base.h"
#include "Pipe.h"
#include "Point3i.h"
#include "FittingPipe.h"

// Statics
GLuint FittingPipe::listCylinder = 0;
GLuint FittingPipe::listSphere = 0;
bool FittingPipe::initialized = false;
GLUquadric * FittingPipe::quadric = gluNewQuadric();

// Constructor
FittingPipe::FittingPipe(Point3i from, Point3i to) {
    _from = from;
    _to = to;
    
    if (!initialized) {
        initialized = true;
        
        gluQuadricNormals(quadric, GLU_SMOOTH);
        gluQuadricTexture(quadric, GL_FALSE);
        
        listCylinder = glGenLists(1);
        glNewList(listCylinder, GL_COMPILE);
        const float center_x = 0;
        const float center_y = 0;
        const float radius = 0.45;
        const float z_low = 0;
        const float z_high = 0.5;
        const float n = 20;
        
        const float theta = 2 * PI / float(n);  // angle for each side-segment
        const float c = cosf(theta);  //precalculate the sine and cosine
        const float s = sinf(theta);
        float t;
        float x = radius; //we start at angle = 0
        float y = 0;
        
        glBegin(GL_TRIANGLE_FAN);
        glNormal3f(center_x, center_y, 1);
        glVertex3f(center_x, center_y, z_high);
        for (int i=0; i <= n; ++i) {
            glVertex3d(x + center_x, y + center_y, z_high);
            t = x;
            x = c * x - s * y;
            y = s * t + c * y;
        }
        glEnd();
        
        x = radius;
        y = 0;
        glBegin(GL_TRIANGLE_STRIP);
        for (int i=0; i <= n; ++i) {
            glNormal3f(x, y, 0);
            glVertex3f(x + center_x, y + center_y, z_low);
            glVertex3f(x + center_x, y + center_y, z_high);
            t = x;
            x = c * x - s * y;
            y = s * t + c * y;
        }
        glEnd();
        glEndList();
        
        listSphere = glGenLists(1);
        glNewList(listSphere, GL_COMPILE);
        gluSphere(quadric, 0.45, 20, 10);
        glEndList();
    }
}

void FittingPipe::draw() {
    glShadeModel( GL_SMOOTH );
    
    glColor3f(_color.r(), _color.g(), _color.b());
    glCallList(listSphere);
    
    rotateDraw(_from);
    rotateDraw(_to);
}

void FittingPipe::rotateDraw(Point3i v) {
    glPushMatrix();
    if (v.z() == 1) {
        // all good
    } else if (v.z() == -1) {
        glRotatef(180, 0, 1, 0);
    } else if (v.x() == 1) {
        glRotatef(90, 0, 1, 0);
    } else if (v.x() == -1) {
        glRotatef(270, 0, 1, 0);
    } else if (v.y() == 1) {
        glRotatef(270, 1, 0, 0);
    } else if (v.y() == -1) {
        glRotatef(90, 1, 0, 0);
    }
    glCallList(listCylinder);
    glPopMatrix();
}
