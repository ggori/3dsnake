#include <stdlib.h>
#include <string.h>
#include "CCanvas.h"
#include "Base.h"
#include "ShaderManager.h"

#ifdef __APPLE__
#include <OpenGL/glu.h>
std::string SHADERS_PATH = "../../shaders/";
#else
#define GL_GLEXT_PROTOTYPES
#include <GL/glu.h>
#include <GL/glcorearb.h>
std::string SHADERS_PATH = "./shaders/";
#endif

using namespace std;


void CCanvas::resizeGL(int width, int height) {
    // set up the window-to-viewport transformation
    glViewport( 0,0, width,height );
    
    // vertical camera opening angle
    double beta = 90.0;
    
    // aspect ratio
    double gamma;
    if (height > 0)
        gamma = width/(double)height;
    else
        gamma = width;
    
    // front and back clipping plane at
    double n = -0.2;
    double f = -CUBE_SIZE;
    
    // frustum corners
    double t = -tan(beta * 3.14159 / 360.0) * n;
    double b = -t;
    double r = gamma * t;
    double l = -r;
    
    // set projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum( l,r , b,t , -n,-f );
    
    // alternatively, directly from alpha and gamma
    //  glPerspective( beta, gamma, -n, -f );
}

// Initialization
void CCanvas::initializeGL() {
    // Shaders
    string vertexshader = "vert.vs";
    string fragmentshader = "frag.fs";
    program = ShaderManager::loadShader((SHADERS_PATH + vertexshader).c_str(), (SHADERS_PATH + fragmentshader).c_str() );
    
    glClearColor(0.7f, 0.7f, 1.0f, 1.0f); // Background
    glClearDepth(1.0f); // Depth Buffer Setup
    glEnable(GL_DEPTH_TEST); // Enables Depth Testing
    glDepthFunc(GL_LEQUAL);	// The Type Of Depth Testing To Do
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Really Nice Perspective Calculations
    
    // fog, basically just pass parameter for the fragment shader, which will do the fog calculations.
    GLfloat fog_color[4] = {0.7f, 0.7f, 1.0f, 1.0f};
    glFogfv(GL_FOG_COLOR, fog_color); // fog color
    glFogf(GL_FOG_DENSITY, FOG_DENSITY); // how dense the fog will be
    glFogi(GL_FOG_MODE, GL_EXP2); // fog mode, can choose between GL_EXP, GL_EXP2, GL_LINEAR
    glHint(GL_FOG_HINT, GL_DONT_CARE); // fog hint, can choose between GL_FASTEST, GL_NICEST, GL_DONT_CARE
    glEnable(GL_FOG);
    
    // lighting
    GLfloat ambient0[] = { 0.2, 0.2, 0.2, 1.0 };
    GLfloat diffuse0[] = { 0.5, 0.5, 0.5, 0.5 };
    GLfloat position0[] = { 2.0, 0.0, 1.0, 0.0 }; //w=0 to set that the light is directional
    glShadeModel( GL_FLAT );
    glLightfv( GL_LIGHT0, GL_AMBIENT, ambient0 );
    glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse0 );
    glLightfv( GL_LIGHT0, GL_POSITION, position0 );
    
    float reduce_factor = 0.5;
    GLfloat ambient1[] = { 0.2*reduce_factor, 0.2*reduce_factor, 0.2*reduce_factor, 1.0 };
    GLfloat diffuse1[] = { 1.0*reduce_factor, 1.0*reduce_factor, 1.0*reduce_factor, 1.0 };
    GLfloat position1[] = { -2.0, 2.0, 1.0, 0.0 };
    glLightfv( GL_LIGHT1, GL_AMBIENT, ambient1 );
    glLightfv( GL_LIGHT1, GL_DIFFUSE, diffuse1 );
    glLightfv( GL_LIGHT1, GL_POSITION, position1 );
    
    GLfloat position2[] = { -2.0, -1.0, -6.0, 0.0 };
    glLightfv( GL_LIGHT2, GL_AMBIENT, ambient1 );
    glLightfv( GL_LIGHT2, GL_DIFFUSE, diffuse1 );
    glLightfv( GL_LIGHT2, GL_POSITION, position2 );
    
    GLfloat position3[] = { 2.0, -2.0, -6.0, 0.0 };
    glLightfv( GL_LIGHT3, GL_AMBIENT, ambient0 );
    glLightfv( GL_LIGHT3, GL_DIFFUSE, diffuse0 );
    glLightfv( GL_LIGHT3, GL_POSITION, position3 );
    
    //needed at least 2 lights to avoid black face
    glEnable( GL_LIGHTING );
    glEnable( GL_LIGHT0 );
    glEnable( GL_LIGHT1 );
    //    glEnable( GL_LIGHT2 );
    //    glEnable( GL_LIGHT3 );
    
    glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE ) ;
    glEnable ( GL_COLOR_MATERIAL ) ;
    
    glPolygonMode(GL_FRONT, GL_FILL);
    
    fullscreened = false;
    shaderActive = false;
    
    CCanvas::resetWorld();
}

void CCanvas::resetWorld() {
    // populate the objects array
    for (int x = 0; x < CUBE_SIZE; ++x) {
        for (int y = 0; y < CUBE_SIZE; ++y) {
            for (int z = 0; z < CUBE_SIZE; ++z) {
                objects[x][y][z] = new Empty();
            }
        }
    }
    
    for (int i=0; i<PICKUPS_QUANTITY; ++i) {
        spawnPickup();
    }
    
    // initial position
    position[0] = CUBE_SIZE / 2;
    position[1] = CUBE_SIZE / 2;
    position[2] = CUBE_SIZE / 2;
    
    // initial speed
    direction[0] = 0;
    direction[1] = 0;
    direction[2] = -1;
    
    // initial up vector
    up[0] = 0;
    up[1] = 1;
    up[2] = 0;
    
    soFarSoGood = 0;
    
    state = MOVING;
    
    lives = MAX_LIVES;
    paused = false;
    score = 0;
    
    dead = false;
    
    density = 0;
}

void CCanvas::spawnPickup() {
    int pos_x, pos_y, pos_z;
    while (true) {
        pos_x = rand() % CUBE_SIZE;
        pos_y = rand() % CUBE_SIZE;
        pos_z = rand() % CUBE_SIZE;
        
        if (objects[pos_x][pos_y][pos_z]->isPickup()) continue;
        
        if (abs(position.x() - pos_x) <= 2
            && abs(position.y() - pos_y) <= 2
            && abs(position.z() - pos_z) <= 2)
            continue;
        break;
    }
    
    // Delete everything around
    for (int x = pos_x - SPAWN_RANGE; x <= pos_x + SPAWN_RANGE; ++x) {
        for (int y = pos_y - SPAWN_RANGE; y <= pos_y + SPAWN_RANGE; ++y) {
            for (int z = pos_z - SPAWN_RANGE; z <= pos_z + SPAWN_RANGE; ++z) {
                
                if( objects[(x + CUBE_SIZE) % CUBE_SIZE]
                    [(y + CUBE_SIZE) % CUBE_SIZE]
                   [(z + CUBE_SIZE) % CUBE_SIZE]->isObstacle() ) {
                    
                    delete objects[(x + CUBE_SIZE) % CUBE_SIZE]
                    [(y + CUBE_SIZE) % CUBE_SIZE]
                    [(z + CUBE_SIZE) % CUBE_SIZE];

                    objects[(x + CUBE_SIZE) % CUBE_SIZE]
                    [(y + CUBE_SIZE) % CUBE_SIZE]
                    [(z + CUBE_SIZE) % CUBE_SIZE] = new Empty();

                }
            }
        }
    }
    
    // place Pickup
    delete objects[pos_x][pos_y][pos_z];
    objects[pos_x][pos_y][pos_z] = new PickupCube();
}

// Invoked when a key is pressed
void CCanvas::keyPressEvent(QKeyEvent * event) {
    
    switch (event->key()) {
        case Qt::Key_Right:
        case Qt::Key_D:
            // rotate direction around the up-vector
            if (!paused && state == MOVING) {
                old_direction = direction;
                direction = direction ^ up;
                state = TURNING;
                soFarSoGood = 0;
            }
            break;
            
        case Qt::Key_Left:
        case Qt::Key_A:
            // rotate direction around the up-vector
            if (!paused && state == MOVING) {
                old_direction = direction;
                direction = up ^ direction;
                state = TURNING;
                soFarSoGood = 0;
            }
            break;
            
        case Qt::Key_Up:
        case Qt::Key_W:
            // rotate both direction and up-vector around the right vector
            if (!paused && state == MOVING) {
                old_direction = direction;
                direction = up;
                up = -old_direction;
                state = TURNING;
                soFarSoGood = 0;
            }
            break;
            
        case Qt::Key_Down:
        case Qt::Key_S:
            // rotate both direction and up-vector around the right vector
            if (!paused && state == MOVING) {
                old_direction = direction;
                direction = -up;
                up = old_direction;
                state = TURNING;
                soFarSoGood = 0;
            }
            break;
            
        case Qt::Key_Space:
        case Qt::Key_Escape: 
            if (!dead) {
                paused = !paused;
            }
            break;
            
        case Qt::Key_R:
            if (dead) {
                // reset
                for (int x = 0; x < CUBE_SIZE; ++x) {
                    for (int y = 0; y < CUBE_SIZE; ++y) {
                        for (int z = 0; z < CUBE_SIZE; ++z) {
                            delete objects[x][y][z];
                        }
                    }
                }
                resetWorld();
            }
            break;
            
        case Qt::Key_F:
            fullscreened = !fullscreened;
            if (fullscreened) {
                window()->showFullScreen();
            } else {
                window()->showNormal();
            }
            break;

        case Qt::Key_Q:
            if (paused) {
                exit(0);
            }
            break;
            
        case Qt::Key_1:
            shaderActive = !shaderActive;
            if (shaderActive) {
                glUseProgramObjectARB(program);
            } else {
                glUseProgramObjectARB(0);
            }
            return;
            
        default:
            return;
    }
}

// Invoked by timer
void CCanvas::paintGL() {
    update();
    
    draw();
    
//    if (!paused)
    tau += 1.0;
}

void CCanvas::update() {
    
    if (!paused) {
        soFarSoGood += 0.05;
    }
    
    if (soFarSoGood > 1) {
        soFarSoGood -=1;
        if (state == TURNING) {
            
            // create trail
            delete objects[position.x()][position.y()][position.z()];
            objects[position.x()][position.y()][position.z()] = new FittingPipe(-old_direction, direction);
            
            state = MOVING;
            
        } else if (state == MOVING) {
            
            // create trail
            Axis_t axis;
            if (direction.x())
                axis = AXIS_X;
            else if (direction.y())
                axis = AXIS_Y;
            else
                axis = AXIS_Z;
            delete objects[position.x()][position.y()][position.z()];
            objects[position.x()][position.y()][position.z()] = new StraightPipe(axis);
        }
        // update position
        position[0] = (position[0] + direction[0] + CUBE_SIZE) % CUBE_SIZE;
        position[1] = (position[1] + direction[1] + CUBE_SIZE) % CUBE_SIZE;
        position[2] = (position[2] + direction[2] + CUBE_SIZE) % CUBE_SIZE;
        
        // collision detection
        if (objects[position.x()][position.y()][position.z()]->exists()) {
            
            if (objects[position.x()][position.y()][position.z()]->isObstacle()) {
                --lives;
                if (lives <= 0) {
                    dead = true;
                    paused = true;
                }
            } else {
                score += density * 1000;
                if (lives < MAX_LIVES) {
                    ++lives;
                }
                CCanvas::spawnPickup();
            }
            
            delete objects[position.x()][position.y()][position.z()];
            objects[position.x()][position.y()][position.z()] = new Empty();
        }
    }
    
    // update density
    density = getDensity();
}

float CCanvas::getDensity() {
    int count = 0;
    for (int x = 0; x < CUBE_SIZE; ++x) {
        for (int y = 0; y < CUBE_SIZE; ++y) {
            for (int z = 0; z < CUBE_SIZE; ++z) {
                if (objects[x][y][z]->isObstacle()) {
                    ++count;
                }
            }
        }
    }
    return (float)count / (CUBE_SIZE * CUBE_SIZE * CUBE_SIZE);
}

void CCanvas::draw() {
    Point3d camera = Point3d();
    Point3d lookAt = Point3d();
    if (state == TURNING) {
        camera[0] += position[0] + (direction[0] * soFarSoGood / 2);
        camera[1] += position[1] + (direction[1] * soFarSoGood / 2);
        camera[2] += position[2] + (direction[2] * soFarSoGood / 2);
        
        // interpolate old direction with new one
        lookAt[0] += position[0] + soFarSoGood * direction[0] + (1-soFarSoGood) * old_direction[0];
        lookAt[1] += position[1] + soFarSoGood * direction[1] + (1-soFarSoGood) * old_direction[1];
        lookAt[2] += position[2] + soFarSoGood * direction[2] + (1-soFarSoGood) * old_direction[2];
    } else if (state == MOVING) {
        camera[0] += position[0] + direction[0] * (soFarSoGood - 0.5);
        camera[1] += position[1] + direction[1] * (soFarSoGood - 0.5);
        camera[2] += position[2] + direction[2] * (soFarSoGood - 0.5);
        
        lookAt[0] += position[0] + direction[0];
        lookAt[1] += position[1] + direction[1];
        lookAt[2] += position[2] + direction[2];
    }
    
    // clear screen and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // set model-view matrix
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    if (paused) {
        gluLookAt(camera.x() + 3 * sin(tau/100), camera.y() + 1.5, camera.z() + 3 * cos(tau/100),
                  camera.x(), camera.y(), camera.z(),
                  0, 1, 0);
    } else {
        glRotated(sin(tau*0.1)*.1, direction.x(), direction.y(), direction.z());
        gluLookAt(camera.x(), camera.y(), camera.z(),
                  lookAt.x(), lookAt.y(), lookAt.z(),
                  up.x(), up.y(), up.z());
    }
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    if (paused) {
        gluLookAt(camera.x() + 3 * sin(tau/100), camera.y() + 1.5, camera.z() + 3 * cos(tau/100),
                  camera.x(), camera.y(), camera.z(),
                  0, 1, 0);
    } else {
        glRotated(sin(tau*0.1)*.1, direction.x(), direction.y(), direction.z());
        gluLookAt(camera.x(), camera.y(), camera.z(),
                  lookAt.x(), lookAt.y(), lookAt.z(),
                  up.x(), up.y(), up.z());
    }
    
    float lowX  = position.x() - CUBE_SIZE;
    float highX = position.x() + CUBE_SIZE;
    float lowY  = position.y() - CUBE_SIZE;
    float highY = position.y() + CUBE_SIZE;
    float lowZ  = position.z() - CUBE_SIZE;
    float highZ = position.z() + CUBE_SIZE;
    
    if (paused) {
        glPushMatrix();
        // TODO: change from FittingPipe to something else better.
        glTranslatef(position.x(), position.y(), position.z());
        HeadPipe::draw(-direction);
        glPopMatrix();
    } else if (state == MOVING) {
        if      (direction.x() ==  1) lowX  = position.x();
        else if (direction.x() == -1) highX = position.x();
        else if (direction.y() ==  1) lowY  = position.y();
        else if (direction.y() == -1) highY = position.y();
        else if (direction.z() ==  1) lowZ  = position.z();
        else if (direction.z() == -1) highZ = position.z();
    }
    
    glTranslatef(lowX, 0, 0);
    for (int x = lowX; x <= highX; ++x) {
        
        glPushMatrix();
        glTranslatef(0, lowY, 0);
        for (int y = lowY; y <= highY; ++y) {
            
            glPushMatrix();
            glTranslated(0, 0, lowZ);
            for (int z = lowZ; z <= highZ; ++z) {
                
                Object * object = objects[(x + CUBE_SIZE) % CUBE_SIZE]
                [(y + CUBE_SIZE) % CUBE_SIZE]
                [(z + CUBE_SIZE) % CUBE_SIZE];
                
                if (object->exists()) {
                    object->draw();
                }
                glTranslatef(0, 0, 1);
            }
            glPopMatrix();
            glTranslatef(0, 1, 0);
        }
        glPopMatrix();
        glTranslatef(1, 0, 0);
    }
    
    
    // Draw Interface
    if (shaderActive) glUseProgramObjectARB(0);
    
    glLoadIdentity();

    int text_size = 20;
    QFont font("DejaVu", text_size);
    
    CCanvas::drawText(50, 25, QString("Lives:"), font);
    CCanvas::drawText(120, 25, QString::number(lives), font);
    
    CCanvas::drawText(50, 50, QString("Score:"), font);
    CCanvas::drawText(120, 50, QString::number(score), font);
    
    QString d;
    d.sprintf("%.1f %%", density*100);
    CCanvas::drawText(50, 75, QString("Density"), font);
    CCanvas::drawText(120, 75, d, font);
    
    if (dead || paused) {
        const QFont bigFont("DejaVu", 32);
        const QFontMetrics metrics(font);
        const QFontMetrics big_metrics(bigFont);
        
        const int center_x = window()->width() / 2;
        const int center_y = window()->height() / 2;
    
        if (dead) {
            const QString game_over("GAME OVER");
            const QString restart("Press R to restart");
            
            const int game_over_w = big_metrics.width(game_over);
            const int restart_w = metrics.width(restart);
    
            CCanvas::drawText(center_x - game_over_w/2, center_y - 10, game_over, bigFont);
            CCanvas::drawText(center_x - restart_w/2, center_y + 20, restart, font);
        } else if (paused) {
            
            const QString paused("PAUSED");
            const QString resume("Press Space Bar to resume");
            
            const int paused_w = big_metrics.width(paused);
            const int resume_w = metrics.width(resume);

            CCanvas::drawText(center_x - paused_w/2, center_y - 10, paused, bigFont);
            CCanvas::drawText(center_x - resume_w/2, center_y + 20, resume, font);
        }
    }

    if (shaderActive) glUseProgramObjectARB(program);
    
    assert(glGetError()==0);
}

// Draw nice text with shadow
void CCanvas::drawText(int x, int y, QString text, QFont font) {
    glColor3f(.6, .6, .6);
    this->renderText(x+1, y+1, text, font);
    glColor3f(1, 1, 1);
    this->renderText(x, y, text, font);
}

