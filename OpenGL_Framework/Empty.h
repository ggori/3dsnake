#ifndef GLRender_Empty_h
#define GLRender_Empty_h

#include "Object.h"
#include "Color.h"

class Empty: public Object {
public:
    Empty()
        : Object(Color3d()) {}
    
    bool exists() { return false; }
    
    bool isObstacle() { return false; }
    
    bool isPickup() { return false; }
    
    void draw() { // drawing an empty forces a crash
        assert(false);
    }
};

#endif
