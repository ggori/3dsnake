#ifndef GLRender_PickupSphere_h
#define GLRender_PickupSphere_h

#include "Pickup.h"

#ifdef __APPLE__
    #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif

class PickupSphere: public Pickup {
private:
    GLUquadric * quadric;
public:
    PickupSphere() {
        quadric = gluNewQuadric();
    }
    
    void draw() {
        glColor3f(_color.r(), _color.g(), _color.b());
        gluSphere(quadric, 0.5, 20, 10);
    }
};

#endif
