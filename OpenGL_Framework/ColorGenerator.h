#ifndef Color_generator_h
#define Color_generator_h

#include "Color.h"

class ColorGenerator {
private:
    static float tau;
    
public:
    // Generate a nice Color3d
    static Color3d generate_color();
    
    static Color3d get_current_color();
};

#endif
