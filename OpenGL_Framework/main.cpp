#include <QApplication>
#include "GLRender.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    GLRender viewer( 0, Qt::Window );
    
    app.setActiveWindow( &viewer );
    viewer.show();
    return app.exec();
}

