varying vec3 normal;
varying vec3 light0;
varying vec3 light1;

void main()
{
	// get normal in world coordinates
	normal = gl_NormalMatrix * gl_Normal;
	
	// get light direction in camera coordinates
	light0 = (vec4(gl_TextureMatrix[0] * gl_LightSource[0].position)).xyz;
	light1 = (vec4(gl_TextureMatrix[0] * gl_LightSource[1].position)).xyz;
	
	gl_FrontColor = gl_Color;
	gl_Position = ftransform();
}
