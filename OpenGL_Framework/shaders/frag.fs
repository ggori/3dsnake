varying vec3 normal;
varying vec3 light0;
varying vec3 light1;

void main()
{
	// ==== Toon Shading ====	
	float intensity0 = dot(normalize(light0), normalize(normal));
	float intensity1 = dot(normalize(light1), normalize(normal));
	if (intensity0 < 0.0) intensity0 = 0.0;
	if (intensity1 < 0.0) intensity1 = 0.0;
	float intensity = intensity0 * 0.7 + intensity1 * 0.7;
	const vec4 base_color = vec4(0.3, 0.3, 0.3, 1.0);
	vec4 toon_color;
	if (intensity > 0.9)
		toon_color = gl_Color;
	else if (intensity > 0.7)
		toon_color = mix(base_color, gl_Color, 0.9);
	else if (intensity > 0.6)
		toon_color = mix(base_color, gl_Color, 0.7);
	else if (intensity > 0.4)
		toon_color = mix(base_color, gl_Color, 0.55);
	else if (intensity > 0.2)
		toon_color = mix(base_color, gl_Color, 0.4);
	else
		toon_color = mix(base_color, gl_Color, 0.2);

	// ==== Fog ====
	const float LOG2 = 1.442695;
	float z = gl_FragCoord.z / gl_FragCoord.w; // distance from camera
	float fogFactor = exp2( -gl_Fog.density * gl_Fog.density * z * z * LOG2 );
	fogFactor = clamp(fogFactor, 0.0, 1.0);

	// interpolate between gl_Color and fog
	gl_FragColor = mix(gl_Fog.color, toon_color, fogFactor );
}
